# printfun -- PWN -- TUCTF2019

[Source](https://gitlab.com/tuctf2019/pwn/printfun)

## Chal Info

Desc: `I have made an impenetrable password checker. Just try your luck!`

Given files:

* printfun

Hints:

* Format strings can be extremely powerful.

Flag: `TUCTF{wh47'5_4_pr1n7f_l1k3_y0u_d01n6_4_b1n4ry_l1k3_7h15?}`

## Deployment

[Docker Hub](https://hub.docker.com/r/asciioverflow/printfun)

Ports: 8888

Example usage:

```bash
docker run -d -p 127.0.0.1:8888:8888 asciioverflow/printfun:tuctf2019
```
